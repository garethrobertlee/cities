## To Prepare

```
bundle install
bundle exec rake db:create
bundle exec rake db:migrate
bundle exec rake db:seed
```

## To Start

```
rails server -b 0.0.0.0 -p 3003
```

## To Reset

```
bundle exec rake db:drop
```