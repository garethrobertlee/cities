class CreateCities < ActiveRecord::Migration[5.1]
  def change
    create_table :cities do |t|
      t.integer :population
      t.string :name
      t.boolean :considering

      t.timestamps
    end
  end
end
