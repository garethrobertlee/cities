# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)
City.create(name: 'Bergen', population: 250_000)
City.create(name: 'Plovdiv', population: 150_000)
City.create(name: 'Chengdu', population: 7_650_000)
City.create(name: 'Recife', population: 2_650_000)
City.create(name: 'Sheffield', population: 465_000)
City.create(name: 'Ufa', population: 665_000)
City.create(name: 'Pittsburgh', population: 440_000)
City.create(name: 'Sarajevo', population: 225_000)
City.create(name: 'Taipei', population: 4_210_000)
City.create(name: 'Newcastle', population: 237_000)
City.create(name: 'Kryvyi Rih', population: 388_000)
City.create(name: 'Trabzon', population: 181_000)
