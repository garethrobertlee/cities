# Cities

![screnshot](/screenshot.jpeg)

# TLDR

### Standalone version

React application is in /web, and Rails application is in /api

Starts up the Rails API - at http://localhost:3003/cities

Starts up the React application - at http://localhost:3401/

React makes api request from Rails API. Cors enabled in Rails

### Docker version

Starts up both applications, accessed at http://localhost:3100. Nginx proxies - no requirement for CORS

-------------------------------------------------------

# USAGE (Standalone)

## Rails

```
cd api
cat README.md
```

## React

```
cd web
cat README.md
```

-------------------------------------------------------

# PREREQUISITIES (for docker version)

```
brew install docker
brew install docker-compose
make
```

-------------------------------------------------------
# USAGE (with docker)

```
make prepare
```

Creates and seeds the database and installs packages

```
make start
```

Starts up the site at - at http://localhost:3100

React makes api request from Rails API via nginx proxy. CORS not needed. React app is visible at http://localhost:3100 and the JSON API on same port at http://localhost:3100/cities

```
make reset
```

resets database

-------------------------------------------------------
# TROUBLESHOOTING

* 502 Bad Gateway

Give it a minute!



