const merge = require('webpack-merge');
const baseConfig = require('./base.config.js');
var path = require('path');

module.exports = merge(baseConfig(), {

    output: {
      publicPath: "/webpack",
      filename: 'bundle.js',
    },
    devtool: 'source-map',
    devServer: {
      port: 3401,
      host: '0.0.0.0',
      disableHostCheck: true,
      watchOptions: {
        poll: 100,
      },
      historyApiFallback: {
        rewrites: [{
          from: /./,
          to: '/src/index.html'
        }]
      },
    }
  })