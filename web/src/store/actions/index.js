const API_URL = window.location.port === "3401" ? 
  `http://localhost:3003/cities.json` :
  `${window.location.href}/cities.json`

export const getCities = () => {
  console.log(API_URL)
  const endpoint = `${API_URL}`
  return dispatch => {
    return fetch(endpoint)
    .then(response => response.json())
    .then(result => dispatch(setCities(result)))
    .catch(error => console.log(error))
  }
}

export const setCities = cities => {
  return {
    type: 'GET_CITIES_SUCCESS',
    cities,
  }
}

const showCity = city => {
  return {
    type: 'CHOSE_A_CITY',
    city,
  }
}

export const sortCities = () => {
  return (dispatch, getState) => {
    const cities = getState().data.cities.sort((a, b) => b.population - a.population)
    dispatch(setCities(cities))
  }
}

export const chooseCity = id => {
  return (dispatch, getState) => {
    const city = getState().data.cities.filter(e => e.id === id)[0]
    dispatch(showCity(city))
  }
}