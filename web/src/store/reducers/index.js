const initialState = {
  cities: [],
  chosenCity: null
}

export default (state = initialState, action) => {
  switch (action.type) {
    case 'GET_CITIES_SUCCESS':
      return Object.assign({}, state, {
        cities: action.cities,
        first: action.cities[0],
      })
    case 'CHOSE_A_CITY':
      return Object.assign({}, state, {
        chosenCity: action.city
      })
    default:
      return state;
  }
}