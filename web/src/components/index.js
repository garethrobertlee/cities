import React from 'react';
import { connect } from 'react-redux';
import styled from 'styled-components'
import { getCities, sortCities } from '../store/actions';
import City from './City'
import CityDetail from './ChosenCity'

const ButtonRow = styled.div`
  margin-left: 10%;
  margin-top: 20px;`

const ButtonWrapper = styled.div`
  display: inline;
  margin-right: 25px;`

const Button = styled.button`
  background: #484AB3;
  color: #F9F7EE;
  border-radius: 8px;
  padding: 10px;
  height: 45px;
  font-size: inherit;`

const CitiesWrapper = styled.div`
  margin-left: 10%;
  width: 80%;
  display:flex;
  margin-top: 20px;
  flex-wrap: wrap;`

const DetailWrapper = styled.div`
  margin-top: 30px;`

const App = props => (
  <div>
    <ButtonRow>
      <ButtonWrapper>
        <Button onClick={() => props.getData()}>Get Data</Button>
      </ButtonWrapper>
      <ButtonWrapper>
        {props.cities.length > 0 && <Button onClick={() => props.sortData()}>Sort</Button>}
      </ButtonWrapper>
    </ButtonRow>
    <CitiesWrapper>
      {props.cities.map(city => <City key={city.id} city={city} chosenCity={props.chosenCity} /> )}
    </CitiesWrapper>
    <DetailWrapper>
      {props.chosenCity && <CityDetail city={props.chosenCity} />}
    </DetailWrapper>
  </div>
);

const mapStateToProps = state => ({
  cities: state.data.cities,
  chosenCity: state.data.chosenCity,
  first: state.data.first
})

const mapDispatchToProps = dispatch => ({
  getData: () => {
    dispatch(getCities())
  },
  sortData: () => {
    dispatch(sortCities())
  },
})

export default connect(mapStateToProps, mapDispatchToProps)(App);

