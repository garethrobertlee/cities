import React from 'react'
import styled, { css } from 'styled-components'
import { connect } from 'react-redux';
import { chooseCity } from '../../store/actions'

const CityName = styled.div`
  min-width: 88px;
  display:flex;
  margin-right: 5px;
  background: #484AB3;
  color: #F9F7EE;
  border-radius: 8px;
  border: 2px solid;
  padding: 10px;
  padding-left:4px;
  padding-right:4px;
  font-size: inherit;
  justify-content: space-around;
  margin-top:5px;
  ${props => props.active && css`
    color: #484AB3;
    background: #F9F7EE;
  `}`

const City = props => (
  <CityName 
    active={props.city === props.chosenCity}
    onClick={() => props.chooseCity(props.city.id)}>
    {props.city.name}
  </CityName>
)

const mapDispatchToProps = dispatch => ({
  chooseCity: (id) => {
    dispatch(chooseCity(id))
  },
})

export default connect(null, mapDispatchToProps)(City);

