import React from 'react'
import styled from 'styled-components'

const Container = styled.div`
  margin-left: 10%;
  width: 80%;
  border: 2px solid #484AB3;
  padding-top: 3%;
`

const CityName = styled.div`
 color: #484AB3;
 margin-bottom: 24px;
 font-family: roboto;
 font-size: 24px;
 padding-left: 20px;
 text-align: center;
`

const ChosenCity = props => (
  <Container>
    <CityName>{props.city.name}</CityName>
    <CityName>Population: {props.city.population.toLocaleString('en')}</CityName>
    {props.city.blurb && <CityName>Blurb: {props.city.blurb}</CityName>}
  </Container>
)

export default ChosenCity

