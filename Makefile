.PHONY : stop start prepare reset clean

prepare:
	docker-compose run api bundle exec rake db:create && docker-compose run api bundle exec rake db:migrate && docker-compose run api bundle exec rake db:seed && cd web && npm i

stop:
	kill -9 $$(lsof -i tcp:3003 | grep LISTEN | awk '{print $$2}') & kill -9 $$(lsof -i tcp:3401 | grep LISTEN | awk '{print $$2}') & rm api/tmp/pids/server.pid

start:
	make stop & rm -f api/tmp/pids/server.pid && docker-compose up

reset:
	docker-compose down && docker-compose run api bundle exec rake db:drop DISABLE_DATABASE_ENVIRONMENT_CHECK=1

clean:
	rm api/tmp/pids/server.pid

zreset:
	make stop & cd api && bundle exec rake db:drop 

zprepare:
	cd api && bundle install && bundle exec rake db:create && bundle exec rake db:migrate && bundle exec rake db:seed && cd ../web && npm i

zstart:
	rm api/tmp/pids/server.pid & cd api && rails server -b 0.0.0.0 -p 3003 -d && cd ../web && npm run dev &